def test_alpine_os(host):
    distribution = host.system_info.distribution
    assert distribution == "alpine"

def test_nginx_is_installed(host):
    nginx = host.package("nginx")
    assert nginx.is_installed
    assert nginx.version.startswith("1.16")
